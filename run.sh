#!/bin/bash
export RAILS_ENV=production
# Migrate DB
RAILS_ENV=production bundle exec rake db:create db:migrate
RAILS_ENV=production bundle exec rake assets:precompile

fix-permissions ./

set +e
[[ -d ./tmp ]] && chgrp -R 0 ./tmp && chmod -R g+rw ./tmp
[[ -d ./db ]] && chgrp -R 0 ./db && chmod -R g+rw ./db
set -e

exec bundle exec "puma --config /etc/puma.cfg"