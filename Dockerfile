FROM centos/ruby-24-centos7

# Set up Discourse
RUN git clone --depth=1 --branch stable https://github.com/discourse/discourse.git ./discourse

WORKDIR /opt/app-root/src/discourse
RUN /bin/bash -c "/opt/rh/rh-ruby24/root/bin/bundle install --no-cache --no-prune --deployment --without development:test"
RUN /bin/bash -c "/opt/rh/rh-ruby24/root/bin/bundle clean -V"

COPY ["discourse.conf","./config/"]
COPY ["puma.cfg","/etc/"]

ADD run.sh ./run.sh

EXPOSE 8080 6379

ENTRYPOINT ["./run.sh"]
